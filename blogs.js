var mongo = require('mongodb');

var MongoClient = mongo.MongoClient,
    URI = "mongodb://datdinhmeganet:datdinh123@ds016298.mlab.com:16298/mynewblogdb";

//db.open might not work with the newest mongoDB. In terminal,
//"npm uninstall mongodb --save"
//"npm install mongodb@2.2.33 --save" 

exports.findAll = function (req, res) {
    MongoClient.connect(URI, function (err, db) {
        if (err) throw err;
        console.log('Get all.');
        db.collection('mynewblogs', function (err, collection) {
            collection.find().toArray(function (err, items) {
                console.log(items);
                res.send(items);//items[0] if you want to find them from the order
            });
        });
    });
};

exports.findByID = function (req, res) {
    MongoClient.connect(URI, function (err, db) {
        if (err) throw err;
        console.log('Getting: ' + req.params.id);
        db.collection('mynewblogs', function (err, collection) {
            collection.findOne({ "_id": new mongo.ObjectId(req.params.id) }, function (err, items) {
                if (items == null) {
                    console.log('No article with that ID.');
                    res.send('No article with that ID.');
                } else {
                    console.log(items);
                    res.send(items);
                }
            });
        });
    });
};

exports.deleteArticle = function (req, res) {
    MongoClient.connect(URI, function (err, db) {
        if (err) throw err;
        console.log('Deleting ID: ' + req.params.id);
        db.collection('mynewblogs', function (err, collection) {
            collection.remove({ '_id': new mongo.ObjectID(req.params.id) }, { safe: true }, function (err, result) {
                if (result[Object.keys(result)[0]]["n"] == 0) {
                    console.log('No article with that ID.');
                } else {
                    console.log('Deleted.');
                }
                res.destroy();//stop the loop?
            });
        });
    });
}

exports.addArticle = function (req, res) {
    MongoClient.connect(URI, function (err, db) {
        if (err) throw err;
        console.log('Adding: ' + JSON.stringify(req.body));
        db.collection('mynewblogs', function (err, collection) {
            collection.insert(req.body, { safe: true }, function (err, result) {
                console.log('Added.');
            });
        });
    });
}

exports.updateArticle = function (req, res) {
    MongoClient.connect(URI, function (err, db) {
        if (err) throw err;
        var article = req.body;
        console.log('Updating ID. ' + req.params.id + ' with ' + JSON.stringify(article));
        db.collection('mynewblogs', function (err, collection) {
            /*First param = the ID of the article to be updated. Second param = the update to apply*/
            collection.update({ '_id': new mongo.ObjectID(req.params.id) }, article, { safe: true }, function (err, result) { });
            collection.findOne({ '_id': new mongo.ObjectId(req.params.id) }, function (err, data) {
                console.log('\n Updated: \n', data);
                res.send(data);
            });
        });
    });
}

/*
Get all blogs:
curl -i -X GET http://localhost:3000/blogs

Get blog with _id value of 5069b47aa892630aae000007:
curl -i -X GET http://localhost:3000/blogs/5069b47aa892630aae000007

Delete blog with _id value of 5069b47aa892630aae000007:
curl -i -X DELETE http://localhost:3000/blogs/5069b47aa892630aae000007

Add a new blog:
curl -i -X POST -H 'Content-Type: application/json' -d '{"name": "New blog", "year": "2009"}' http://localhost:3000/blogs

Modify blog with _id value of 5069b47aa892630aae000007:
curl -i -X PUT -H 'Content-Type: application/json' -d '{"name": "New blog", "year": "2010"}' http://localhost:3000/blogs/5069b47aa892630aae000007 
*/

