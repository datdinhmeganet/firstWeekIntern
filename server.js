var express = require('express'),
    blogs = require('./blogs');

var app = express();

app.set('port', (process.env.PORT || 5000));


app.configure(function (){
  app.use(express.logger('dev')); //default, short, tiny, dev
  app.use(express.bodyParser());
});

app.get('/blogs', blogs.findAll);
app.get('/blogs/:id', blogs.findByID);
app.post('/blogs', blogs.addArticle);
app.put('/blogs/:id', blogs.updateArticle);
app.delete('/blogs/:id', blogs.deleteArticle)

app.listen(app.get('port'), function() {
  console.log('App is running, server is listening on port ', app.get('port'));
});
//app.listen(3000);
//console.log('Listening on port 3000...');
