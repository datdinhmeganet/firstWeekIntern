
API to view database
=======
## Live version
Live version of the API can be found [here](https://peaceful-wave-36891.herokuapp.com/blogs).

## Useful requests:

- Get all blogs:

```javascript 
curl -i -X GET https://peaceful-wave-36891.herokuapp.com/blogs
```

- Get blog with _id value of 5b065f38fb6fc075763f8b9b:

```javascript
curl -i -X GET https://peaceful-wave-36891.herokuapp.com/blogs/5b065f38fb6fc075763f8b9b
```

- Delete blog with _id value of 5b065f38fb6fc075763f8b9b:

```javascript
curl -i -X DELETE https://peaceful-wave-36891.herokuapp.com/blogs/5b065f38fb6fc075763f8b9b
```

- Add a new blog:

```javascript
curl -i -X POST -H 'Content-Type: application/json' -d '{"name": "New blog", "year": "2009"}' https://peaceful-wave-36891.herokuapp.com/blogs
```

- Modify blog with _id value of 5b065f38fb6fc075763f8b9b:

```javascript
curl -i -X PUT -H 'Content-Type: application/json' -d '{"name": "New blog", "year": "2010"}' https://peaceful-wave-36891.herokuapp.com/blogs/5b065f38fb6fc075763f8b9b
```   